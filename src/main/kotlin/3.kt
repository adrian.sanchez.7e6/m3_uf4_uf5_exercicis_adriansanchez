import javax.print.attribute.standard.MediaSizeName.A

abstract class Instrument(){
    abstract val resonancia: Any
    abstract fun makeSounds(repeticion: Int)
}

class Triangle(override val resonancia: Any): Instrument() {
    override fun makeSounds(repeticion: Int) {
        val sound = when (resonancia){
            1 -> "TINC"
            2 -> "TIINC"
            3 -> "TIIINC"
            4 -> "TIIIINC"
            5 -> "TIIIIINC"
            else -> "Resonancia incorrecta"
        }
        repeat(repeticion){
            println(sound)
        }
    }
}

class Drump(override val resonancia: Any): Instrument() {
    override fun makeSounds(repeticion: Int) {
        val sound = when (resonancia){
            "A" -> "TAAAM"
            "O" -> "TOOOM"
            "U" -> "TUUUM"
            else -> "Resonancia incorrecta"
        }
        repeat(repeticion){
            println(sound)
        }
    }
}

fun main(args: Array<String>) {
    val instruments: List<Instrument> = listOf(
        Triangle(5),
        Drump("A"),
        Drump("O"),
        Triangle(1),
        Triangle(5)
    )
    play(instruments)
}

private fun play(instruments: List<Instrument>) {
    for (instrument in instruments) {
        instrument.makeSounds(1)
    }
}