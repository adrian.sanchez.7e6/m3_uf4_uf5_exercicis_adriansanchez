abstract class Pregunta(val text: String) {
    abstract fun comprobarRespuesta(respuesta: String): Boolean
}

class FreeTextQuestion(text: String, private val respuestaCorrecta: String) : Pregunta(text) {
    override fun comprobarRespuesta(respuesta: String): Boolean {
        return respuesta == respuestaCorrecta
    }
}

class MultipleChoiceQuestion(text: String, private val choices: List<String>, private val posicionCorrecta: Int) : Pregunta(text) {
    override fun comprobarRespuesta(respuesta: String): Boolean {
        return respuesta.toIntOrNull() == posicionCorrecta
    }

    fun mostrarOpciones() {
        for ((index, respuesta) in choices.withIndex()) {
            println("$index) $respuesta")
        }
    }
}

class Quiz(private val questions: List<Pregunta>) {
    fun run() {
        var correctas = 0
        for (question in questions) {
            println(question.text)
            when (question) {
                is FreeTextQuestion -> {
                    val respuesta = readLine() ?: ""
                    if (question.comprobarRespuesta(respuesta)) {
                        correctas++
                    }
                }
                is MultipleChoiceQuestion -> {
                    question.mostrarOpciones()
                    val respuesta = readLine() ?: ""
                    if (question.comprobarRespuesta(respuesta)) {
                        correctas++
                    }
                }
            }
        }
        println("Tienes $correctas de ${questions.size} correctas!")
    }
}
fun main(args: Array<String>) {
    val preguntas = Quiz(
        listOf(
            FreeTextQuestion("En que país se encuentra Copenhague?", "Dinamarca"),
            MultipleChoiceQuestion("Cuál es el país con más población del mundo?",
                listOf("India", "China", "Brasil", "Rusia"), 1),
            FreeTextQuestion("Cuál es el planeta más grande del sistema solar?", "Júpiter")
        )
    )
    preguntas.run()
}