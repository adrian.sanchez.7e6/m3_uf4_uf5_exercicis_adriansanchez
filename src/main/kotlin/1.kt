class Student(val name: String, val textGrade: String)

fun main() {
    val alumne1 = Student("Mar", "FAILED")
    val alumne2 = Student("Joan", "EXCELLENT")
    println("Nom de l'estudiant: ${alumne1.name}, Nota de l'estudiant: ${alumne1.textGrade}")
    println("Nom de l'estudiant: ${alumne2.name}, Nota de l'estudiant: ${alumne2.textGrade}")
}