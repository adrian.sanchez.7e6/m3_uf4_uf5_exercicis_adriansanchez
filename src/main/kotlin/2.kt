class ColoresArcoiris {
    val colores = listOf("rojo", "naranja", "amarillo", "verde", "azul", "añil", "violeta")

    fun contains(color: String?): Boolean {
        return colores.contains(color?.toLowerCase())
    }
}

fun main() {
    val arcoiris = ColoresArcoiris()

    print("Introduce un color: ")
    val color = readLine()

    if (arcoiris.contains(color)) {
        println("$color sí está en el arcoiris")
    } else {
        println("$color no está en el arcoíris")
    }
}