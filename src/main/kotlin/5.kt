import java.util.Scanner

fun main() {
    val reader = GymControlManualReader()
    val totesLesID = mutableMapOf<String, Boolean>()


    println("Introdueix les ID's dels usuaris")
    for (i in 1..8) {
        val id = reader.nextId()
        if (totesLesID.containsKey(id) && totesLesID[id] == true) {
            println("$id Sortida")
            totesLesID[id] = false
        } else {
            println("$id Entrada")
            totesLesID[id] = true
        }
    }
}

interface GymControlReader {
    fun nextId(): String
}

class GymControlManualReader(val scanner: Scanner = Scanner(System.`in`)) : GymControlReader {
    override fun nextId() = scanner.next()
}